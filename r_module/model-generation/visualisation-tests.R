
file <- read.csv(file = "C:/Users/Greed/IdeaProjects/wpmg3/r_module/model-generation/training-sets/apache-mezos-training-sets3.csv", head=TRUE, sep=";")

summary(file)
names(file)
file$x1
plot(file$x1)

library(ggplot2)
qplot(x1,x2 / x3, isBugProne, data = file)

qplot('color', x2 / x1, data = file, geom = "boxplot")

qplot(x1/x2,isBugProne, data = file, colour = 'color')

qplot(x1, data = file, geom = "histogram")

qplot(x1, data = file, geom = "density")

qplot(x1, isBugProne, data = file, geom = "line")

ck_metrics=c("x1","x2","x3","x4") 

qplot(x1, data=file, geom="density", fill=x2, alpha=I(.5), 
      main="Distribution of Gas Milage", xlab="Miles Per Gallon", 
      ylab="Density")

qplot(x1, x2, data=file,
      facets=x1~x4, size=I(3),
      xlab="Horsepower", ylab="Miles per Gallon") 

qplot(x1, x2, data=file,
      facets=x1~x2, size=I(5),
      xlab="Horsepower", ylab="Miles per Gallon") 

qplot(x1,isBugProne, data=file, geom=c("point", "smooth"), 
      method="lm", formula=y~x, color="cyl", 
      main="Regression of MPG on Weight", 
      xlab="Weight", ylab="Miles per Gallon")


ggplot(file) + 
  geom_jitter(aes(x1,x2), colour="blue") + geom_smooth(aes(x1,x2), method=lm, se=FALSE) +
  geom_jitter(aes(x2,x3), colour="green") + geom_smooth(aes(x2,x3), method=lm, se=FALSE) +
  geom_jitter(aes(x3,x4), colour="red") + geom_smooth(aes(x3,x4), method=lm, se=FALSE) +
  labs(x = "Percentage cover (%)", y = "Number of individuals (N)")


ggplot(file) + 
  geom_jitter(aes(x1,isBugProne), colour="blue") + geom_smooth(aes(x1,isBugProne), method=lm, se=FALSE) +
  geom_jitter(aes(x2,isBugProne), colour="green") + geom_smooth(aes(x2,isBugProne), method=lm, se=FALSE) +
  geom_jitter(aes(x3,isBugProne), colour="red") + geom_smooth(aes(x3,isBugProne), method=lm, se=FALSE) +
  geom_jitter(aes(x4,isBugProne), colour="yellow") + geom_smooth(aes(x4,isBugProne), method=lm, se=FALSE) +
  labs(x = "Percentage cover (%)", y = "Number of individuals (N)")


require(ggplot2)
require(reshape2)
mtcars2 = melt(mtcars, id.vars='mpg')
ggplot(mtcars2) +
  geom_jitter(aes(value,mpg, colour=variable),) + geom_smooth(aes(value,mpg, colour=variable), method=lm, se=FALSE) +
  facet_wrap(~variable, scales="free_x") +
  labs(x = "Percentage cover (%)", y = "Number of individuals (N)")


#dobry wykres, jak rozkładają się poszczególne
require(ggplot2)
require(reshape2)
file2 = melt(file, id.vars='isBugProne')
ggplot(file2) +
  geom_jitter(aes(value,isBugProne, colour=variable),) + geom_smooth(aes(value,isBugProne, colour=variable), method=lm, se=FALSE) +
  facet_wrap(~variable, scales="free_x") +
  labs(x = "Percentage cover (%)", y = "Number of individuals (N)")

#lepszy wykres,
require(ggplot2)
require(reshape2)
file2 = melt(file, id.vars='isBugProne')
ggplot(file2) +
  geom_jitter(aes(value,isBugProne, colour=variable)) + geom_smooth(aes(value,isBugProne), method=lm, se=FALSE) +
  facet_wrap(~variable, scales="free_x") +
  labs(x = "Percentage cover (%)", y = "Number of individuals (N)")


pie(file$x1)

install.packages("visreg")
install.packages("svm")

library(randomForest)
library(visreg)
library(svm)
aq <- na.omit(file)
fit1 <- randomForest(x1  ~  x4 +  soc + ccrc, data=aq)
#fit2 <- svm(x1 ~ x4 +  soc + ccrc, data=aq)
visreg(fit1,  ylab="Ozone", ylim=c(0, 150))
#visreg(fit2,  ylab="Ozone", ylim=c(0, 150))




