package org.pwr.metric.mbrc;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Test;
import org.mockito.Mockito;
import org.pwr.model.ClassEntity;
import org.pwr.repository.IClassEntityRepository;

public class MeanBugRatioMetricTest {

	private String revision = "";
	private int daysToSkip = 0;
	private int minNumberOfCommits = 0;

	private String commiter1 = "commiter1";
	private String commiter2 = "commiter2";
	private String commiter3 = "commiter3";

	private double commiter1ratio = 0.25;
	private double commiter2ratio = 0.1;
	private double commiter3ratio = 0.15;

	private long revisionDate = 2;

	@Test
	public void shouldEvaluateCorrectMBRCValue() {

		// GIVEN
		IClassEntityRepository mockClassEntityRepository = Mockito.mock(IClassEntityRepository.class);

		ClassEntity classentity = new ClassEntity();
		Set<String> commiters = new HashSet<>();
		commiters.add(commiter2);
		commiters.add(commiter3);

		Map<String, Double> commitersToRatios = new HashMap<>();
		commitersToRatios.put(commiter1, commiter1ratio);
		commitersToRatios.put(commiter3, commiter2ratio);
		commitersToRatios.put(commiter2, commiter3ratio);

		List<ClassEntity> classentities = new ArrayList<>();
		classentities.add(classentity);

		MeanBugRatioMetricParameters params = new MeanBugRatioMetricParameters(minNumberOfCommits, daysToSkip,
				revision);

		MeanBugRatioMetric mbrm = new MeanBugRatioMetric(mockClassEntityRepository, classentities, params);

		// WHEN
		when(mockClassEntityRepository.findOutDateForRevision(revision)).thenReturn(revisionDate);
		when(mockClassEntityRepository.getCommitters(classentity)).thenReturn(commiters);
		when(mockClassEntityRepository.findOutCommitersRatiosToDate(revisionDate, minNumberOfCommits, daysToSkip))
				.thenReturn(commitersToRatios);
		mbrm.initializeCommonFields();
		mbrm.calculate(classentity);

		// THEN
		assertEquals(classentity.getMbrcScore(), 0.125, 0.000000);

	}

}
