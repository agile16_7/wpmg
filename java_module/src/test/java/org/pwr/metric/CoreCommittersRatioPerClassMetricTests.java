package org.pwr.metric;

import static org.junit.Assert.*;
import java.util.*;

import javafx.util.Pair;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.pwr.metric.ccrc.CoreCommittersRatioPerClassMetric;
import org.pwr.model.ClassEntity;
import org.pwr.repository.IClassEntityRepository;

public class CoreCommittersRatioPerClassMetricTests {
    private String coreCommitter = "Core";
    private String regularCommitter = "NotCore";
    private List<ClassEntity> classEntities;
    private long currentTime;
    private Set<String> committers;
    private List<Pair<String, Integer>> committersWithCommits;
    private ClassEntity currentEntity;
    private IClassEntityRepository classEntityRepository;


    @Before
    public void setup(){
        currentTime = new Date().getTime();
        this.setupClassEntities();

        this.setupCommitters();
        this.setupCommittersWithCommits();

        classEntityRepository = Mockito.mock(IClassEntityRepository.class);
        Mockito.when(classEntityRepository.getCommitters(Matchers.any())).thenReturn(this.committers);
        Mockito.when(classEntityRepository.getNumberOfCommits(Matchers.anyLong())).thenReturn(this.classEntities.size());
        Mockito.when(classEntityRepository.getCommittersWithNumberOfCommits(Matchers.anyLong())).thenReturn(this.committersWithCommits);
    }

    private void setupCommittersWithCommits() {
        this.committersWithCommits = new LinkedList<>();
        this.committersWithCommits.add(new Pair<>(coreCommitter, 4));
        this.committersWithCommits.add(new Pair<>(regularCommitter, 1));
    }

    private void setupCommitters() {
        committers = new HashSet<>();
        committers.add(coreCommitter);
        committers.add(regularCommitter);
    }

    private void setupClassEntities() {
        this.classEntities = new ArrayList<>();

        currentEntity = new ClassEntity();
        currentEntity.setCommitAuthor(coreCommitter);
        currentEntity.setCommitDate(currentTime);

        classEntities.add(currentEntity);
        ClassEntity classEntity2 = new ClassEntity();
        classEntity2.setCommitAuthor(coreCommitter);
        classEntity2.setCommitDate(currentTime - 1000);
        classEntities.add(classEntity2);

        ClassEntity classEntity3 = new ClassEntity();
        classEntity3.setCommitAuthor(coreCommitter);
        classEntity3.setCommitDate(currentTime - 2000);
        classEntities.add(classEntity3);

        ClassEntity classEntity4 = new ClassEntity();
        classEntity4.setCommitAuthor(coreCommitter);
        classEntity4.setCommitDate(currentTime - 3000);
        classEntities.add(classEntity4);

        ClassEntity classEntity5 = new ClassEntity();
        classEntity5.setCommitAuthor(regularCommitter);
        classEntity5.setCommitDate(currentTime - 4000);
        classEntities.add(classEntity5);
    }

    @Test
    public void testCalculate() {
        LinkedList<ClassEntity> currentRevisionEntities = new LinkedList<>();
        currentRevisionEntities.add(currentEntity);
        CoreCommittersRatioPerClassMetric metric = new CoreCommittersRatioPerClassMetric(this.classEntityRepository, currentRevisionEntities);

        double expected = 0.5;
        metric.calculate();
        double actual = currentEntity.getCcrcScore();

        double delta = 0.00001;
        assertEquals(expected, actual, delta);

    }
}
