package org.pwr.metric;

import java.util.LinkedList;
import java.util.List;

import org.pwr.metric.ccrc.CoreCommittersRatioPerClassMetric;
import org.pwr.metric.mbrc.MeanBugRatioMetric;
import org.pwr.metric.mbrc.MeanBugRatioMetricParameters;
import org.pwr.model.ClassEntity;
import org.pwr.repository.ClassEntityRepository;

public class CommitterMetricCalculator implements IMetricCalculator {

	@Override
	public void calculateMetricsForRevision(String revision, List<ClassEntity> classEntities) {
		LinkedList<ClassMetricBase> metrics = new LinkedList<>();

		// should be taken from .properties
		int mbrcSkippedDays = 0;
		int mbrcMinNumbOfCommits = 10;

		MeanBugRatioMetricParameters mbrcParams = new MeanBugRatioMetricParameters(mbrcMinNumbOfCommits,
				mbrcSkippedDays, revision);

		metrics.add(new CoreCommittersRatioPerClassMetric(new ClassEntityRepository(), classEntities));
		metrics.add(new MeanBugRatioMetric(new ClassEntityRepository(), classEntities, mbrcParams));

		metrics.parallelStream().forEach(ClassMetricBase::calculate);
	}
}
