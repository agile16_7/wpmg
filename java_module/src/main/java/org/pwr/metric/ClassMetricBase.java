package org.pwr.metric;

import org.pwr.model.ClassEntity;
import org.pwr.repository.IClassEntityRepository;

import java.util.List;

public abstract class ClassMetricBase {

    protected IClassEntityRepository classEntityRepository;
    protected List<ClassEntity> entitiesList;

    public ClassMetricBase(IClassEntityRepository classEntityRepository, List<ClassEntity> entitiesList){
        this.classEntityRepository = classEntityRepository;
        this.entitiesList = entitiesList;
    }

    public void calculate(){
        if(this.entitiesList != null && !this.entitiesList.isEmpty()){
            this.initializeCommonFields();
            entitiesList.parallelStream().forEach(this::calculate);
        }
    }

    abstract protected void initializeCommonFields();
    abstract protected void calculate(ClassEntity classEntity);

}
