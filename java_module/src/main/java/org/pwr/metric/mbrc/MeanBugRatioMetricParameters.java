package org.pwr.metric.mbrc;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class MeanBugRatioMetricParameters {

	/**
	 * Tells how many commits developer must create to be taken into account in
	 * this metric.
	 */
	private int minimalNumberOfCommits = 10;

	/**
	 * Days to skip from the very first commit date.
	 */
	private int daysToSkipp = 60;

	/**
	 * Revision SH1
	 */
	private String revision;

}
