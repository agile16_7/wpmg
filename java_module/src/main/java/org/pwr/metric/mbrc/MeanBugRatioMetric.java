package org.pwr.metric.mbrc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.pwr.metric.ClassMetricBase;
import org.pwr.model.ClassEntity;
import org.pwr.repository.IClassEntityRepository;

import lombok.Getter;
import lombok.Setter;

/**
 * Represents MBRC metric implementation. Provides method to calculate value of
 * metric for revision. Score is mean bug ratio for all commiters of the class,
 * which committed in the whole project over selected number of commits after
 * chosen period of time from first commit. Both values are given in class
 * constructor. {@link minimalNumberOfCommitsPerInterval} are omitted.
 *
 * @author Grzegorz
 *
 */

@Getter
@Setter
public class MeanBugRatioMetric extends ClassMetricBase {

	/**
	 * Mapping of commiters to their bug ratios in project.
	 */
	private Map<String, Double> commitersToRatios;

	/**
	 * Tells how many commits developer must create to be taken into account in
	 * this metric.
	 */
	private int minimalNumberOfCommits = 10;

	/**
	 * Days to skip from the very first commit date.
	 */
	private int daysToSkipp = 60;

	/**
	 * Revision SH1
	 */
	private String revision;

	/**
	 * Create new MeanBugRatioMetric representation.
	 * 
	 * @param classEntityRepository
	 *            repository connecting to database.
	 * @param entitiesList
	 *            classentites, for which the metrics will be evaluated.
	 * @param metricParams
	 *            parameters of the metric, including days to be skipped and
	 *            minimal number of commits of developer in project.
	 */
	public MeanBugRatioMetric(IClassEntityRepository classEntityRepository, List<ClassEntity> entitiesList,
			MeanBugRatioMetricParameters metricParams) {
		this(classEntityRepository, entitiesList);

		this.minimalNumberOfCommits = metricParams.getMinimalNumberOfCommits();
		this.daysToSkipp = metricParams.getDaysToSkipp();
		this.revision = metricParams.getRevision();
	}

	private MeanBugRatioMetric(IClassEntityRepository classEntityRepository, List<ClassEntity> entitiesList) {
		super(classEntityRepository, entitiesList);
	}

	/**
	 * {@inheritDoc}}
	 */
	@Override
	protected void initializeCommonFields() {
		commitersToRatios = findOutRatiosOfCommiters(findOutDateOfRevision(this.revision));
	}

	/**
	 * {@inheritDoc}}
	 */
	@Override
	protected void calculate(ClassEntity classEntity) {
		Set<String> committers = this.classEntityRepository.getCommitters(classEntity);
		Map<String, Double> copyOfCommitersToRatios = new HashMap<String, Double>(commitersToRatios);

		for (Iterator<Entry<String, Double>> it = copyOfCommitersToRatios.entrySet().iterator(); it.hasNext();) {
			Map.Entry<String, Double> entry = it.next();
			if (!committers.contains(entry.getKey())) {
				it.remove();
			}
		}

		double mbrcScore = new ArrayList<Double>(copyOfCommitersToRatios.values()).stream().mapToDouble(a -> a)
				.average().orElse(-1);
		classEntity.setMbrcScore(mbrcScore);
	}

	private Map<String, Double> findOutRatiosOfCommiters(long toDate) {
		return classEntityRepository.findOutCommitersRatiosToDate(toDate, minimalNumberOfCommits, daysToSkipp);
	}

	private long findOutDateOfRevision(String revision) {
		return classEntityRepository.findOutDateForRevision(revision);
	}

}
