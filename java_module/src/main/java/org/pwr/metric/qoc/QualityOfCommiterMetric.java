package org.pwr.metric.qoc;

import org.pwr.metric.ClassMetricBase;
import org.pwr.model.ClassEntity;
import org.pwr.repository.IClassEntityRepository;
import org.pwr.tool.JDBCConnector;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class QualityOfCommiterMetric extends ClassMetricBase {


    private ClassEntity classEntity;

    private HashMap<String, Double> QCofAllCommiters;

    public QualityOfCommiterMetric(IClassEntityRepository classEntityRepository, List<ClassEntity> entitiesList) {
        super(classEntityRepository, entitiesList);
    }

    @Override
    protected void initializeCommonFields() {

        //Select author, count(*) FROM classentity group by author
        HashMap<String, Integer> NAofAllCommiters = new HashMap<>();
        try {
            Statement stmt = JDBCConnector.getInstance().getConnection().createStatement();
            String sql = "Select author, count(*) FROM classentity group by author";
            ResultSet rs = stmt.executeQuery(sql);
            while(rs.next()){
                NAofAllCommiters.put(rs.getString(1), rs.getInt(2));
            }
            stmt.close();
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        //Select author, count(*) FROM classentity WHERE isBugProne = 1 group by author
        HashMap<String, Integer> NDofAllCommiters = new HashMap<>();
        try {
            Statement stmt = JDBCConnector.getInstance().getConnection().createStatement();
            String sql = "Select author, count(*) FROM classentity WHERE isBugProne = 1 group by author";
            ResultSet rs = stmt.executeQuery(sql);
            while(rs.next()){
                NDofAllCommiters.put(rs.getString(1), rs.getInt(2));
            }
            stmt.close();
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        for(String commiter : NAofAllCommiters.keySet()){
            double QC;

            if(NDofAllCommiters.containsKey(commiter)){
                int ND = NDofAllCommiters.get(commiter);
                int NA = NAofAllCommiters.get(commiter);

                QC = 1 - (ND/NA);

            } else {
                QC = 1;
            }
            QCofAllCommiters.put(commiter, QC);
        }
    }

    @Override
    public void calculate(ClassEntity classEntity) {
        this.classEntity = classEntity;
        List<String> commiters = getAllClassCommiters(classEntity.getPath());
        double upper = 0;
        for(String commiter: commiters){
            upper += QCofAllCommiters.get(commiter)*calculateNC(classEntity.getPath(), commiter);
        }

        System.out.println("Result for " + classEntity.getPath() + ": "+ classEntity.getCommitHash());
        System.out.println(upper/calculateNAC(classEntity));
    }

    private int calculateNAC(ClassEntity classEntity){
        int result = 1;
        //Select count(*) FROM classentity WHERE filePath = 'spring-web/src/main/java/org/springframework/web/server/ServerWebExchange.java'
        try {
            Statement stmt = JDBCConnector.getInstance().getConnection().createStatement();
            String sql = "Select count(*) FROM classentity WHERE filePath = '"+classEntity.getPath()+"'";
            ResultSet rs = stmt.executeQuery(sql);
            while(rs.next()){
                result = rs.getInt(1);
            }
            stmt.close();
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    private int calculateNC(String filePath, String commmiter){

        //Select count(*) FROM classentity WHERE filePath = 'spring-web/src/test/java/org/springframework/web/bind/WebExchangeDataBinderTests.java' AND author = 'Rossen Stoyanchev'
        int result = 0;
        try {
            Statement stmt = JDBCConnector.getInstance().getConnection().createStatement();
            String sql = "Select count(*) FROM classentity WHERE filePath = '"+filePath+"' AND author = '"+commmiter+"'";
            ResultSet rs = stmt.executeQuery(sql);
            while(rs.next()){
                result = rs.getInt(1);
            }
            stmt.close();
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }


    private List<String> getAllClassCommiters(String filePath){

        //Select author FROM classentity WHERE filePath = 'spring-web/src/test/java/org/springframework/web/bind/WebExchangeDataBinderTests.java'
        List<String> result = new LinkedList<>();
        try {
            Statement stmt = JDBCConnector.getInstance().getConnection().createStatement();
            String sql = "Select author FROM classentity WHERE filePath = '"+filePath+"'";
            ResultSet rs = stmt.executeQuery(sql);
            while(rs.next()){
                result.add(rs.getString(1));
            }
            stmt.close();
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
}
