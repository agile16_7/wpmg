package org.pwr.metric.ccrc;

import javafx.util.Pair;
import org.pwr.metric.ClassMetricBase;
import org.pwr.model.ClassEntity;
import org.pwr.repository.IClassEntityRepository;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Maciej Wolański
 * maciekwski@gmail.com
 * on 25.01.2017.
 */
public class CoreCommittersRatioPerClassMetric extends ClassMetricBase {
    private Set<String> coreCommitters;

    public CoreCommittersRatioPerClassMetric(IClassEntityRepository classEntityRepository, List<ClassEntity> entitiesList) {
        super(classEntityRepository, entitiesList);
    }

    @Override
    protected void initializeCommonFields() {
        coreCommitters = this.findCoreCommittersForRevision();
    }

    @Override
    protected void calculate(ClassEntity classEntity) {
        Set<String> committers = this.classEntityRepository.getCommitters(classEntity);
        Set<String> coreCommittersForEntity = new HashSet<>(committers);
        coreCommittersForEntity.retainAll(coreCommitters);
        double numOfCoreCommitters = coreCommittersForEntity.size();
        classEntity.ccrcScore = numOfCoreCommitters / committers.size();
    }

    private Set<String> findCoreCommittersForRevision() {
        List<Pair<String, Integer>> sortedCommitersWithCommits = classEntityRepository.getCommittersWithNumberOfCommits(entitiesList.get(0).getCommitDate());
        int numOfCommits = classEntityRepository.getNumberOfCommits(entitiesList.get(0).getCommitDate());
        return getTopCommitters(numOfCommits, sortedCommitersWithCommits);
    }

    private Set<String> getTopCommitters(int numOfCommits, List<Pair<String, Integer>> committerWithCommits) {
        Set<String> coreCommitters = new HashSet<>();
        int acc = 0;
        int count = 0;
        int cap = (int) (numOfCommits * 0.7);
        for (; acc < cap && count < committerWithCommits.size(); count++) {
            coreCommitters.add(committerWithCommits.get(count).getKey());
            acc += committerWithCommits.get(count).getValue();
        }
        return coreCommitters;
    }
}