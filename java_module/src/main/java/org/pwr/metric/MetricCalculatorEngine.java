package org.pwr.metric;

import org.pwr.model.ClassEntity;
import org.pwr.repository.IMetricScoreUpdater;
import org.pwr.repository.IStatementFeeder;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Maciej Wolański
 * maciekwski@gmail.com
 * on 21.01.2017.
 */
public class MetricCalculatorEngine extends Thread {

    protected IMetricCalculator metricCalculator;
    protected IMetricScoreUpdater scoreUpdater;
    protected IStatementFeeder feeder;
    protected List<LinkedHashMap<String, List<ClassEntity>>> allClassEntitiesForCalculation;


    public MetricCalculatorEngine(List<LinkedHashMap<String, List<ClassEntity>>> allClassEntitiesForCalculation, IMetricCalculator metricCalculator, IMetricScoreUpdater scoreUpdater, IStatementFeeder feeder){
        this.allClassEntitiesForCalculation = allClassEntitiesForCalculation;
        this.metricCalculator = metricCalculator;
        this.scoreUpdater = scoreUpdater;
        this.feeder = feeder;
    }

    @Override
    public void run() {
        this.allClassEntitiesForCalculation.parallelStream().forEach(this::runForRepo);
    }

    private void runForRepo(LinkedHashMap<String, List<ClassEntity>> al) {
        this.calculateMetricsForAllClassEntities(al);
        this.scoreUpdater.batchUpdateMetrics(al.values().stream().flatMap(Collection::stream).collect(Collectors.toList()), this.feeder);
    }

    protected void calculateMetricsForAllClassEntities(LinkedHashMap<String, List<ClassEntity>> classEntities) {
        for(String hash : classEntities.keySet()){
            metricCalculator.calculateMetricsForRevision(hash, classEntities.get(hash));
        }
    }
}
