package org.pwr.conf;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class SETTINGS {

    private static final Properties CONFIGURATION = new Properties();
    private static FileInputStream INPUT_STREAM;
    private static FileOutputStream OUTPUT_STREAM;


    public static Map<String, String> getRepositories(){
        HashMap<String, String> result = new HashMap<>();
        try {
            INPUT_STREAM = new FileInputStream("application.conf");
            CONFIGURATION.load(INPUT_STREAM);
            INPUT_STREAM.close();
            String repos = CONFIGURATION.getProperty("repositories");
            result = mapRepositories(repos);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
        //OUTPUT_STREAM = new FileOutputStream("application.conf");
    }

    private static HashMap<String, String> mapRepositories(String repos) {
        HashMap<String, String> result = new HashMap<>();
        String[] repoRows = repos.split(";");
        for (String row: repoRows) {
            String[] nameUrl = row.split(",");
            result.put(nameUrl[0], nameUrl[1]);
        }
        return result;
    }

    public static String depressFile(){
        String result = null;
        try {
            INPUT_STREAM = new FileInputStream("application.conf");
            CONFIGURATION.load(INPUT_STREAM);
            INPUT_STREAM.close();
            result = CONFIGURATION.getProperty("depressFile");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    };

    public static boolean isDemo() {
        boolean result = false;
        try {
            INPUT_STREAM = new FileInputStream("application.conf");
            CONFIGURATION.load(INPUT_STREAM);
            INPUT_STREAM.close();
            result = Boolean.getBoolean(CONFIGURATION.getProperty("demomode.isDemo"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static boolean shouldCloneDemo() {
        boolean result = false;
        try {
            INPUT_STREAM = new FileInputStream("application.conf");
            CONFIGURATION.load(INPUT_STREAM);
            INPUT_STREAM.close();
            result = Boolean.getBoolean(CONFIGURATION.getProperty("demomode.cloneRepository"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
    public static boolean shouldCloneFull() {
        boolean result = false;
        try {
            INPUT_STREAM = new FileInputStream("application.conf");
            CONFIGURATION.load(INPUT_STREAM);
            INPUT_STREAM.close();
            result = Boolean.getBoolean(CONFIGURATION.getProperty("full.cloneRepository"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static boolean shouldSaveFull() {
        boolean result = false;
        try {
            INPUT_STREAM = new FileInputStream("application.conf");
            CONFIGURATION.load(INPUT_STREAM);
            INPUT_STREAM.close();
            result = Boolean.getBoolean(CONFIGURATION.getProperty("full.saveToDb"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static boolean shouldSaveDemo() {
        boolean result = false;
        try {
            INPUT_STREAM = new FileInputStream("application.conf");
            CONFIGURATION.load(INPUT_STREAM);
            INPUT_STREAM.close();
            result = Boolean.getBoolean(CONFIGURATION.getProperty("demo.saveToDb"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static boolean shouldCalculateFull() {
        boolean result = false;
        try {
            INPUT_STREAM = new FileInputStream("application.conf");
            CONFIGURATION.load(INPUT_STREAM);
            INPUT_STREAM.close();
            result = Boolean.getBoolean(CONFIGURATION.getProperty("full.calculateMetrics"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static boolean shouldCalculateDemo() {
        boolean result = false;
        try {
            INPUT_STREAM = new FileInputStream("application.conf");
            CONFIGURATION.load(INPUT_STREAM);
            INPUT_STREAM.close();
            result = Boolean.getBoolean(CONFIGURATION.getProperty("demo.calculateMetrics"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
}
