package org.pwr;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.pwr.conf.SETTINGS;
import org.pwr.metric.CKMetricCalculator;
import org.pwr.metric.CommitterMetricCalculator;
import org.pwr.metric.MetricCalculatorEngine;
import org.pwr.metric.CommitterMetricCalculatorEngine;
import org.pwr.model.ClassEntity;
import org.pwr.parser.CommitParser;
import org.pwr.repository.ClassEntityRepository;
import org.pwr.repository.CodeMetricStatementFeeder;
import org.pwr.repository.CommitterMetricStatementFeeder;
import org.pwr.repository.DbMetricScoreUpdater;
import org.pwr.tool.GitTool;
import org.pwr.tool.Utils;

import java.io.*;
import java.util.*;

public class MetricsMain {

    static List<LinkedHashMap<String, List<ClassEntity>>> toCalculateBugs = new LinkedList<>();
    static List<LinkedHashMap<String, List<ClassEntity>>> allClassEntitiesForCalculation = new LinkedList<>();

    public static void loadRepositoriesToDB() throws GitAPIException, IOException {
        if(SETTINGS.isDemo()){
            loadDemoRepositories();
        } else {
            loadFullRepositories();
        }
    }

    private static void loadFullRepositories() throws GitAPIException, IOException{
        boolean shouldClone = SETTINGS.shouldCloneFull();
        boolean shouldSaveToDb = SETTINGS.shouldSaveFull();
        String repoPathPrefix = "Repositories";
        Map<String, String> repositories = SETTINGS.getRepositories();
        for (Map.Entry<String, String> repository : repositories.entrySet()) {
            String path = repoPathPrefix+"/"+repository.getKey();
            if(shouldClone){
                GitTool.cloneRepository(repository.getValue(), path);
            } else {
                GitTool.openRepository(path);
            }
            if(shouldSaveToDb)// TODO: JGIT
                CommitParser.parseToClassEntity(SETTINGS.depressFile(), repository.getKey());
            System.out.println("Class loaded to DB " + repository.getKey());
            Utils.setUpBugProne();  //TODO: PRZEROBIC NA KILKA REPO

            LinkedHashMap<String, List<ClassEntity>> bugClassEntities = ClassEntityRepository.getBugClassEntities(); //TODO: FOR REPO
            toCalculateBugs.add(bugClassEntities);
            allClassEntitiesForCalculation.add(ClassEntityRepository.getAllClassEntitiesForCalculation(bugClassEntities));  //TODO: FOR REPO
        }
    }

    private static void loadDemoRepositories() throws GitAPIException, IOException{

    }

    public static void main(String[] args) throws IOException, GitAPIException {
        loadRepositoriesToDB();
        calculateAllMetricsAndUpdateDb();
    }



    private static void calculateAllMetricsAndUpdateDb() {
        LinkedList<MetricCalculatorEngine> calculationEngines = new LinkedList<>();
        calculationEngines.add(new MetricCalculatorEngine(allClassEntitiesForCalculation, new CKMetricCalculator(), new DbMetricScoreUpdater(), new CodeMetricStatementFeeder()));
        calculationEngines.add(new CommitterMetricCalculatorEngine(allClassEntitiesForCalculation, new CommitterMetricCalculator(), new DbMetricScoreUpdater(), new CommitterMetricStatementFeeder()));
        for(MetricCalculatorEngine mec:calculationEngines){
            mec.start();
            try {
                mec.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
