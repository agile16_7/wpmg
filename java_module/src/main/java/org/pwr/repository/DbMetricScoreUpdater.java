package org.pwr.repository;

import org.pwr.model.ClassEntity;
import org.pwr.tool.JDBCConnector;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

/**
 * Created by Maciej Wolański
 * maciekwski@gmail.com
 * on 25.01.2017.
 */
public class DbMetricScoreUpdater implements IMetricScoreUpdater {

    @Override
    public void batchUpdateMetrics(List<ClassEntity> entities, IStatementFeeder feeder) {
        Connection connection = JDBCConnector.getInstance().getConnection();
        try {
            try {
                connection.setAutoCommit(false);

                Statement statement = connection.createStatement();
                feeder.feedBatchUpdateStatementForMetrics(entities, statement);

                statement.executeBatch();
                connection.commit();
            } catch (Exception e) {
                connection.rollback();
            } finally {
                if (connection != null) {
                    connection.close();
                }
            }
        } catch (SQLException exc){
            exc.printStackTrace();
        }
    }
}
