package org.pwr.repository;

import org.pwr.model.ClassEntity;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

/**
 * Created by Maciej Wolański
 * maciekwski@gmail.com
 * on 28.01.2017.
 */
public interface IStatementFeeder {
    void feedBatchUpdateStatementForMetrics(List<ClassEntity> entities, Statement statement)  throws SQLException;
}
