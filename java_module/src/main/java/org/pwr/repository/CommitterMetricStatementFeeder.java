package org.pwr.repository;

import org.pwr.model.ClassEntity;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

/**
 * Created by Maciej Wolański
 * maciekwski@gmail.com
 * on 28.01.2017.
 */
public class CommitterMetricStatementFeeder implements IStatementFeeder {
    @Override
    public void feedBatchUpdateStatementForMetrics(List<ClassEntity> entities, Statement statement) throws SQLException {
        for(ClassEntity ce : entities){
            String sql = "UPDATE metrics.classentity SET"
                    + " soc = " + ce.getSocScore()
                    + " qofc = " + ce.getQocScore()
                    + " ccrc = " + ce.getCcrcScore()
                    + " mbrc = " + ce.getMbrcScore()
                    + " WHERE sh1 = " + ce.getCommitHash();
            statement.addBatch(sql);
        }
    }
}
