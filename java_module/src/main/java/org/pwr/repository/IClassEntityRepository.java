package org.pwr.repository;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.pwr.model.ClassEntity;

import javafx.util.Pair;

public interface IClassEntityRepository {

	void addClassEntity(ClassEntity classEntity);

	List<ClassEntity> getAllEntities(long lastCommitDate);

	Set<String> getCommitters(ClassEntity classEntity);

	List<Pair<String, Integer>> getCommittersWithNumberOfCommits(long lastCommitDate);

	int getNumberOfCommits(long lastCommitDate);

	Map<String, Double> findOutCommitersRatiosToDate(long toDate, long minCommits, long daysSkipped);

	long findOutDateForRevision(String revision);
}
