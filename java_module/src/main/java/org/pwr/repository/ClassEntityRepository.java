package org.pwr.repository;

import javafx.util.Pair;
import org.pwr.model.ClassEntity;
import org.pwr.tool.JDBCConnector;
import org.pwr.tool.Utils;

import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

//TODO: Przerobić na Beana
public class ClassEntityRepository implements IClassEntityRepository {

	@Override
	public void addClassEntity(ClassEntity classEntity) {
		String sql = "INSERT INTO classentity (sh1, filePath, author, commitDate, action, message, isBugFix) VALUES ('"
				+ classEntity.commitHash + "','" + classEntity.path + "','" + classEntity.commitAuthor + "',"
				+ classEntity.commitDate + ",'" + classEntity.actionType + "','" + classEntity.commitMessage + "',"
				+ classEntity.isBuxFixing().value + ")";
		JDBCConnector.getInstance().insertStatement(sql);
	}

	public static ArrayList<ClassEntity> getBugFixingClasses() {
		ArrayList<ClassEntity> classes = new ArrayList<>();
		try {
			Statement stmt = JDBCConnector.getInstance().getConnection().createStatement();
			String sql = "SELECT * FROM classentity WHERE isBugFix = 1 order by commitDate;";
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				ClassEntity ce = Utils.createClassEntityFromRS(rs);
				// TODO:
				classes.add(ce);
			}
			stmt.close();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return classes;
	}

	public static ClassEntity getPrevoiusTouch(ClassEntity classEntity) {
		ClassEntity ce = null;
		try {
			Statement stmt = JDBCConnector.getInstance().getConnection().createStatement();
			String sql = "SELECT * FROM metrics.classentity WHERE filePath = '" + classEntity.path
					+ "' AND commitDate < " + classEntity.commitDate + " order by commitDate;";
			ResultSet rs = stmt.executeQuery(sql);
			// TODO:

			while (rs.next()) {
				ce = Utils.createClassEntityFromRS(rs);
				return ce;
			}
			stmt.close();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ce;
	}

	// Pobiera listę numerów rewizji w których wystąpiły błędy i liczbę klas *2
	// w których one wystąpiły
	public static LinkedHashMap<String, Integer> getBugRevisions() {
		LinkedHashMap<String, Integer> map = new LinkedHashMap<>();
		try {
			Statement stmt = JDBCConnector.getInstance().getConnection().createStatement();
			String sql = "SELECT count(*), sh1 FROM classentity WHERE isBugProne = 1 group by sh1 order by commitDate";
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				map.put(rs.getString("sh1"), rs.getInt(1) * 2);
			}
			stmt.close();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return map;
	}

	public static LinkedHashMap<String, List<ClassEntity>> getBugClassEntities() {
		LinkedHashMap<String, List<ClassEntity>> map = new LinkedHashMap<>();
		try {
			Statement stmt = JDBCConnector.getInstance().getConnection().createStatement();
			String sql = "SELECT * FROM classentity WHERE isBugProne = 1 group by sh1 order by commitDate";
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				ClassEntity ce = Utils.createClassEntityFromRS(rs);
				if (map.containsKey(ce.commitHash)) {
					map.get(ce.commitHash).add(ce);
				} else {
					List<ClassEntity> list = new LinkedList<>();
					list.add(ce);
					map.put(ce.commitHash, list);
				}
			}
			stmt.close();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return map;
	}

	public static LinkedHashMap<String, List<ClassEntity>> getAllClassEntitiesForCalculation(
			LinkedHashMap<String, List<ClassEntity>> toCalculateBugs) {
		Set<String> revisionNumbers = toCalculateBugs.keySet();
		LinkedHashMap<String, List<ClassEntity>> result = new LinkedHashMap<>();
		for (String commit : revisionNumbers) {
			List<ClassEntity> buggyClasses = toCalculateBugs.get(commit);
			List<ClassEntity> friendlyClasses = new LinkedList<>();
			for (ClassEntity ce : buggyClasses) {
				File f = new File(ce.path);
				String folder = f.getParent();
				try {
					Statement stmt = JDBCConnector.getInstance().getConnection().createStatement();
					String sql = "SELECT * FROM classentity WHERE isBugProne = 2 AND filePath LIKE '" + folder + "%'";
					ResultSet rs = stmt.executeQuery(sql);
					while (rs.next()) {
						ClassEntity candidate = Utils.createClassEntityFromRS(rs);
						if (!friendlyClasses.contains(candidate)) {
							friendlyClasses.add(candidate);
							break;
						}
					}
					stmt.close();
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			friendlyClasses.addAll(buggyClasses);
			result.put(commit, friendlyClasses);
		}
		return result;
	}

	@Override
	public List<ClassEntity> getAllEntities(long lastCommitDate) {
		List<ClassEntity> entities = new LinkedList<>();
		try {
			Statement stmt = JDBCConnector.getInstance().getConnection().createStatement();
			String sql = "SELECT * FROM metrics.classentity" + " WHERE commitDate <= " + lastCommitDate;
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				entities.add(Utils.createClassEntityFromRS(rs));
			}
			stmt.close();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return entities;
	}

	@Override
	public Set<String> getCommitters(ClassEntity classEntity) {
		Set<String> committers = new HashSet<>();
		try {
			Statement stmt = JDBCConnector.getInstance().getConnection().createStatement();
			String sql = "SELECT author FROM classentity" + " WHERE commitDate <= " + classEntity.getCommitDate()
					+ " filePath = " + classEntity.getPath();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				committers.add(rs.getString(1));
			}

			stmt.close();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return committers;
	}

	@Override
	public List<Pair<String, Integer>> getCommittersWithNumberOfCommits(long lastCommitDate) {
		List<Pair<String, Integer>> pairs = new LinkedList<>();
		try {
			Statement stmt = JDBCConnector.getInstance().getConnection().createStatement();
			String sql = "SELECT author, COUNT(*) AS num FROM classentity" + " WHERE commitDate <= " + lastCommitDate
					+ " GROUP BY author" + " ORDER BY num DESC";
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				pairs.add(new Pair<>(rs.getString(1), rs.getInt(2)));
			}

			stmt.close();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return pairs;

	}

	@Override
	public int getNumberOfCommits(long lastCommitDate) {
		try {
			Statement stmt = JDBCConnector.getInstance().getConnection().createStatement();
			String sql = "SELECT COUNT(*) FROM metrics.classentity" + " WHERE commitDate <= " + lastCommitDate;
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				return rs.getInt(1);
			}
			stmt.close();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public Map<String, Double> findOutCommitersRatiosToDate(long toDate, long minCommits, long daysSkipped) {
		Map<String, Double> commitersToRatios = new HashMap<>();

		try {
			Statement stmt = JDBCConnector.getInstance().getConnection().createStatement();

			String sql = "SELECT c.author, SUM(CASE WHEN c.isBugProne =  1 THEN 1 ELSE 0 END)/COUNT(*) 'ratio' FROM classentity c WHERE c.author IN ( SELECT t.author FROM classentity t GROUP BY t.author HAVING COUNT(*) >="
					+ minCommits + ") AND c.commitDate <= " + toDate
					+ " AND c.commitDate >= ((SELECT MIN(x.commitDate) FROM classentity x WHERE x.author = c.author) + ( "
					+ daysSkipped + " * 24 * 60 * 60 * 1000)) GROUP BY c.author";

			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				commitersToRatios.put(rs.getString("author"), rs.getDouble("ratio"));
			}

			stmt.close();
			rs.close();
		} catch (SQLException e) {
		}
		return commitersToRatios;

	}

	@Override
	public long findOutDateForRevision(String revision) {
		long date = -1;

		try {
			Statement stmt = JDBCConnector.getInstance().getConnection().createStatement();

			String sql = "SELECT MAX(c.commitDate) FROM classentity c WHERE c.sh1 = '" + revision + "'";
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				date = rs.getLong("commitDate");
			}

			stmt.close();
			rs.close();
		} catch (SQLException e) {
		}

		return date;
	}
}
