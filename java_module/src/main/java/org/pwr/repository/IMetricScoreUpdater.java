package org.pwr.repository;

import org.pwr.model.ClassEntity;

import java.util.List;
// TODO: TO BEAN
public interface IMetricScoreUpdater {
    void batchUpdateMetrics(List<ClassEntity> entities, IStatementFeeder feeder);
}
