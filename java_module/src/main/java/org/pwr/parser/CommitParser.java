package org.pwr.parser;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.pwr.conf.SETTINGS;
import org.pwr.enums.BugFix;
import org.pwr.model.ClassEntity;
import org.pwr.repository.ClassEntityRepository;
import org.pwr.tool.Utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

public class CommitParser {

    public static ArrayList<ClassEntity> parseToClassEntity(String path, String repoName) throws IOException {
        ClassEntityRepository classEntityRepository = new ClassEntityRepository();
        ArrayList<ClassEntity> classEntities = new ArrayList<>();

        FileInputStream file = new FileInputStream(new File(path));

        XSSFWorkbook workbook = new XSSFWorkbook(file);

        XSSFSheet sheet = workbook.getSheetAt(0);

        Iterator<Row> rowIterator = sheet.iterator();

        int counter = 0;
        while(rowIterator.hasNext()){
            Row row = rowIterator.next();

            String pathToFile = row.getCell(5).getRichStringCellValue().toString();

            if(Utils.isJavaFile(pathToFile)) {
                ClassEntity classEntity = new ClassEntity();
                classEntity.repository = repoName;
                classEntity.commitHash = row.getCell(7).getRichStringCellValue().toString();
                classEntity.commitDate = row.getCell(6).getDateCellValue().getTime();
                classEntity.commitMessage = row.getCell(4).getRichStringCellValue().toString();
                classEntity.commitAuthor = row.getCell(2).getRichStringCellValue().toString();
                classEntity.path = pathToFile;
                classEntity.actionType = row.getCell(3).getRichStringCellValue().toString();

                if (Utils.isBugFix(classEntity.commitMessage)) {
                    classEntity.isFix = BugFix.TRUE;
                } else {
                    classEntity.isFix = BugFix.FALSE;
                }

                //Cut commit message for DB optimisation
                String commitMessage = classEntity.commitMessage;
                commitMessage = commitMessage.substring(0,  Math.min(commitMessage.length(), 10));
                classEntity.commitMessage = commitMessage;

                classEntityRepository.addClassEntity(classEntity);
                System.out.println("Added class " + counter);
//                if(counter++ > SETTINGS.MAX_SIZE){
//                    break;
//                }
            }
        }
        return classEntities;
    }
}
