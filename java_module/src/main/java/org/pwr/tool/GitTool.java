package org.pwr.tool;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;

import java.io.File;
import java.io.IOException;

public class GitTool {



    public static Git openRepository(String directory) throws GitAPIException, IOException {
        return Git.open(new File(directory + "/.git"));
    }

    public static Git cloneRepository(String address, String directory) throws GitAPIException {
        return Git.cloneRepository()
                .setURI(address)
                .setDirectory(new File(directory))
                .call();
    }
}
